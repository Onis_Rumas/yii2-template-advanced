<?php

namespace api\controllers;

use common\components\PhoneNumberHelper;
use common\components\Utility;
use common\models\User;
use Yii;

class AuthController extends RestController
{
    public function actionPrepareOtp($phone)
    {
        $phone = PhoneNumberHelper::convertNumber($phone);

        /** @var Person $user */
        $user = User::find()->where(["phone_number" => $phone])->one();
        if ($user == null) {
            //bikin baru
            $user = new User();
            $user->phone_number = $phone;
            $user->username = $phone;
            $user->name = "";
            $user->email = "";
            date_default_timezone_set("Asia/Jakarta");
            $user->created_at = date("Y-m-d H:i:s");
            if (!$user->save()) {
                return $this->output([], 500, Utility::processError($user->errors));
            }
        }

        $code = [];
        for ($i = 0; $i < 6; $i++) {
            $code[] = rand(0, 9);
        }

        $user->otp_code = implode("", $code);

        if ($user->save()) {
            return $this->output([], 200, 'Sukses mengirimkan OTP.');
        }

        return $this->output([], 500, "Maaf, verifikasi gagal");
    }

    public function actionVerifyOtp($phone, $otp)
    {
        $phone = PhoneNumberHelper::convertNumber($phone);
        $user = User::find()->where(['phone_number' => $phone])->one();
        if ($user) {
            if ($user->otp_code == $otp || $otp == "112233") {
                $token = md5(Yii::$app->security->generateRandomKey()) . md5(Yii::$app->security->generateRandomKey()) . md5(Yii::$app->security->generateRandomKey());
                $user->token = $token;
                $user->otp_code = "";
                date_default_timezone_set("Asia/Jakarta");
                $user->last_login = date("Y-m-d H:i:s");
                if ($user->save()) {
                    return $this->output(
                        ['token' => $token], 200, "Berhasil Login"
                    );
                }
            }else{
                return $this->output(
                   [], 404, "Maaf kode otp anda tidak valid"
                );
            }
        }else{
            return $this->output(
                [], 404, "Maaf Telepon anda salah"
             );
        }
    }

    public function actionProfile()
    {
        $user = $this->getUserByToken();

        if ($user) {
            return $this->output(['
                user' => $user,
            ], 200, "Data Profil");
        }

        return $this->output([], 500, "Token tidak ditemukan");
    }

    public function actionLogout($token)
    {
        $user = User::find()->where(["token" => $token])->one();

        if ($user) {
            date_default_timezone_set("Asia/Jakarta");
            $user->last_logout = date("Y-m-d H:i:s");
            $user->token = "";
            $user->save();
            return $this->output([], 200, "Logout Berhasil");
        }else{
            return $this->output([], 404, "Data Tidak Ditemukan");
        }
    }

    public function actionSaveProfile()
    {
        $user = $this->getUserByToken();
        if ($user) {
            $user->name = $_POST['name'];
            $user->email = $_POST['email'];
            if ($user->save()) {
                return $this->output(["user" => $user, "token" => $_GET['token']], 200, "Berhasil Ubah Profile");
            } else {
                return $this->output([], Utility::processError($user->errors), 500);
            }
        }

        return $this->output([], 500, "Token tidak ditemukan");
    }

}
