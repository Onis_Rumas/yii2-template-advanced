<?php

namespace api\controllers;

use yii\helpers\Json;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use common\models\User;

class RestController extends Controller
{
    public function init()
    {
        ini_set("memory_limit", "4000M");
        set_time_limit(3600);

        date_default_timezone_set("Asia/Jakarta");

        $_GET["_format"] = "json";
        header('Access-Control-Allow-Origin: *');
        parent::init();
    }

    public function beforeAction($action)
    {
        date_default_timezone_set("Asia/Jakarta");
        // ini_set("memory_limit", "4000M");
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return true;
    }

    public function output($data = [], $statusCode = 200, $message = null)
    {
        \Yii::$app->response->setStatusCode($statusCode);

        return [
            "message" => $message,
            "status" => $statusCode,
            "code" => $statusCode,
            "data" => $data
        ];
    }

    public function forceLogout()
    {
        return self::output([], 410, "Maaf, sesi anda telah habis");
    }
    
    public function getUserByToken()
    {
        /** @var User $user */
        $user = User::find()->where(["token" => $_GET['token']])->one();
        if ($user) {
            return $user;
        }
        return null;
    }

    public function getPost(){
        $json = file_get_contents('php://input');
        $obj = [];
        if($json) {
            $obj = json_decode($json, true);
        }
        return $obj;
    }
}