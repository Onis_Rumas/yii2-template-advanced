<?php
/**
 * Created by PhpStorm.
 * User: feb
 * Date: 24/03/18
 * Time: 09.48
 */

namespace api\controllers;


use api\controllers\RestController;
use common\models\Produk;
use common\components\Utility;

class ProdukController extends RestController
{
	public function actionIndex($page = 0)
    {
		$perPage = 5;
		$produk = Produk::find()
            ->limit($perPage)
            ->offset($page * $perPage)->all();
        return $this->output($produk);
    }

    public function actionCreate()
    {
		$produk = new Produk();
		$produk->nama = $_POST["nama"];
		$produk->created_at = date("Y-m-d H:i:s");
		$produk->updated_at = date("Y-m-d H:i:s");
        if ($produk->save()) {
            return $this->output("Berhasil Menyimpan.");
        } else {
            return $this->output(Utility::processError($produk->errors), 500);
        }
    }

    public function actionUpdate($id)
    {
		$produk = Produk::find()->where(["id"=>$id])->one();
		$produk->nama = $_POST["nama"];
		$produk->updated_at = date("Y-m-d H:i:s");
        
        if ($produk->save()) {
            return $this->output("Berhasil Diupdate.");
        } else {
            return $this->output(Utility::processError($produk->errors), 500);
        }
    }

    public function actionDelete($id)
    {
		$produk = Produk::find()->where(["id"=>$id])->one();
		
		if ($produk->delete()) {
            return $this->output("Berhasil Dihapus.");
        } else {
            return $this->output(Utility::processError($produk->errors), 500);
        }
    }
}