-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `controller` varchar(50) NOT NULL,
  `action` varchar(50) NOT NULL DEFAULT 'index',
  `icon` varchar(50) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `menu` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `menu` (`id`, `name`, `controller`, `action`, `icon`, `order`, `parent_id`) VALUES
(1,	'Dashboard',	'site',	'index',	'fas fa-home',	1,	NULL),
(2,	'Master',	'',	'index',	'fa fa-database',	2,	NULL),
(3,	'Menu',	'menu',	'index',	'far fa-circle',	3,	2),
(4,	'Privilege',	'role',	'index',	'far fa-circle',	4,	2),
(5,	'Operator',	'user',	'index',	'far fa-circle',	5,	2);

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `produk`;
CREATE TABLE `produk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `produk` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(3,	'katok',	'2020-02-26 09:01:07',	'2020-02-26 09:01:07'),
(4,	'katok bolong',	'2020-02-26 09:01:21',	'2020-02-26 09:01:21'),
(5,	'katok bolong separuh',	'2020-02-26 09:01:29',	'2020-02-26 09:01:29'),
(6,	'katok bolong tengah',	'2020-02-26 09:01:43',	'2020-02-26 09:01:43'),
(7,	'katok bolong kabeh',	'2020-02-26 09:01:49',	'2020-02-26 09:01:49'),
(8,	'katok bolong kabeh',	'2020-02-26 09:04:45',	'2020-02-26 09:04:45'),
(9,	'katok bolong kabeh',	'2020-02-26 09:05:21',	'2020-02-26 09:05:21');

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `role` (`id`, `name`) VALUES
(1,	'Super Administrator'),
(2,	'Administrator'),
(3,	'Regular User');

DROP TABLE IF EXISTS `role_menu`;
CREATE TABLE `role_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  KEY `menu_id` (`menu_id`),
  CONSTRAINT `role_menu_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  CONSTRAINT `role_menu_ibfk_2` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `role_menu` (`id`, `role_id`, `menu_id`) VALUES
(177,	2,	1),
(178,	2,	2),
(179,	2,	3),
(180,	2,	4),
(181,	2,	5),
(210,	1,	1),
(211,	1,	2),
(212,	1,	3),
(213,	1,	4),
(214,	1,	5),
(215,	3,	1);

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `role_id` int(11) NOT NULL DEFAULT '3',
  `status` int(11) NOT NULL DEFAULT '1',
  `photo_url` varchar(255) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_logout` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user` (`id`, `username`, `password`, `email`, `name`, `role_id`, `status`, `photo_url`, `last_login`, `last_logout`, `created_at`, `updated_at`) VALUES
(1,	'admin',	'21232f297a57a5a743894a0e4a801fc3',	NULL,	'Super Administrator',	1,	1,	'user_photo.jpg',	NULL,	NULL,	'2020-02-21 19:18:42',	'2020-02-21 19:18:42');

-- 2020-04-20 05:55:19
