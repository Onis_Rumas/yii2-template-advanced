<?php

use common\models\Menu;
use yii\helpers\Html;
use \yii\base\Module;
use \yii\bootstrap\ActiveForm;
use \yii\helpers\Inflector;

/**
 * @var yii\web\View $this
 * @var common\models\Role $model
 */

$this->title = 'Hak Akses - ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Hak Akses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => "Set Menu untuk ".$model->name, 'url' => ['view', 'id' => $model->id]];
?>
<?php $form = ActiveForm::begin(['id' => 'my-form']); ?>
<div class="box box-success">
    <div class="box-header">
        <h3 class="box-title">Pilih Menu untuk Hak Akses <?= $model->name; ?></h3>
    </div>
    <div class="box-body">
        <?php
        function isChecked($role_id, $menu_id){
            $role_menu = \common\models\RoleMenu::find()->where(["menu_id"=>$menu_id, "role_id"=>$role_id])->one();
            if($role_menu){
                return TRUE;
            }
            return FALSE;
        }

        function showCheckbox($name, $value, $label, $checked = FALSE){
            ?>
            <label>
                <input type="checkbox" name="<?= $name ?>" value="<?= $value ?>" class="minimal actions" <?= $checked ? "checked" : "" ?>>
            </label>
            <label style="padding: 0px 20px 0px 5px"> <?= $label; ?></label>
            <?php
        }

        function getAllChild($role_id, $parent_id=NULL, $level = 0){
            /** @var Menu $menu */
            foreach(Menu::find()->where(["parent_id"=>$parent_id])->orderBy("`order` ASC")->all() as $menu){
                ?>
                    <div style="padding-left: <?= $level * 20 ?>px">
                        <label>
                            <input type="checkbox" name="menu[]" value="<?= $menu->id ?>" class="minimal" <?= isChecked($role_id, $menu->id) ? "checked" : "" ?>>
                         <?= $menu->name; ?></label>
                    </div>
                <?php

                getAllChild($role_id, $menu->id, $level + 1);
            }
        }

        getAllChild($model->id, NULL);
        ?>

    </div>
    <div class="box-footer">
        <button class="btn btn-info" type="button" id="select_all_btn">
            <i class="fa fa-check"></i> Select/Deselect All
        </button>
        <button class="btn btn-success" type="submit">
            <i class="fa fa-save"></i> Simpan
        </button>
    </div>
</div>
<?php ActiveForm::end(); ?>

<?php $this->registerJs('

$("#select_all_btn").click(function(){
    $(".minimal").iCheck("toggle");
});

$(".select-all").on("ifClicked", function(){

    if($(this).prop("checked")){
        $(this).closest(".form-group").find(".actions").iCheck("uncheck");
    }else{
        $(this).closest(".form-group").find(".actions").iCheck("check");
    }
});

'); ?>
