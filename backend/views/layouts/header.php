<?php

use common\models\User;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

/** @var User $user */
$user = Yii::$app->user->identity;
?>

<style>
  .skin-blue .main-header .navbar .sidebar-toggle:hover {
      background-color: #007DF0;
  }
</style>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">APP</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo', 'style'=>'background-color: #007DFF']) ?>

    <nav class="navbar navbar-static-top" style="background-color: #007DFF;" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <img src="<?= common\components\Photo::get($user->photo_url) ?>" class="user-image" alt="User Image">
                      <span class="hidden-xs"><?= $user->name ?></span>
                    </a>
                    <ul class="dropdown-menu">
                      <!-- User image -->
                      <li class="user-header" style="background-color: #007DFF;">
                        <img src="<?= common\components\Photo::get($user->photo_url) ?>" class="img-circle" alt="User Image">

                        <p>
                          <?= $user->name ?> - <?= $user->role->name ?>
                          <small>Member since <?= date("M. Y", strtotime($user->created_at)) ?></small>
                        </p>
                      </li>
                      <!-- Menu Footer-->
                      <li class="user-footer">
                        <div class="pull-right">
                          <a href="<?=Url::to(['/site/logout'])?>" data-method="post" class="btn btn-default btn-flat">Sign out</a>
                        </div>
                      </li>
                    </ul>
                </li>

                <!-- User Account: style can be found in dropdown.less -->
                <!--
                    <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-cogs"></i></a>
                    </li>
                -->
            </ul>
        </div>
    </nav>
</header>
