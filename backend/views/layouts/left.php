<?php
use common\components\SidebarMenu;
use common\models\User;
use dmstr\widgets\Menu;

/** @var User $user */
$user = Yii::$app->user->identity;
?>
<style>
    .sidebar-menu .svg-inline--fa {
        /*width: 25px !important;*/
    }
</style>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= common\components\Photo::get($user->photo_url) ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?= $user->name ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> <?= $user->role->name ?></a>
            </div>
        </div>

        <?php
        $items = SidebarMenu::getMenu(Yii::$app->user->identity->role_id);
        Menu::$iconClassPrefix = '';
        echo Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => $items["menus"]
            ]
        ) ?>

    </section>

</aside>
