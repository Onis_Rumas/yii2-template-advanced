<?php

use dmstr\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;

/**
 * @var yii\web\View $this
 * @var common\models\User $model
 */

$this->title = 'User ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'View';
?>

<div class="row">
    <div class="col-md-12">
        <p class='pull-left'>
            <?= Html::a('<span class="glyphicon glyphicon-pencil"></span> ' . 'Edit', ['update', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
            
        </p>
        <p class="pull-right">
            <?= Html::a('<span class="glyphicon glyphicon-list"></span> ' . 'List Users', ['index'], ['class' => 'btn btn-default']) ?>
        </p>
    </div>
</div>

<div class="row">
    <div class="col-md-3">
        <div class="box box-primary">
            <div class="box-body box-profile">
                <img class="profile-user-img img-responsive img-circle" src="<?= common\components\Photo::get($model->photo_url) ?>" alt="User profile picture">

                <h3 class="profile-username text-center"><?= $model->name ?></h3>

                <p class="text-muted text-center"><?= $model->role->name ?></p>

                <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                        
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="col-md-9">
      <div class="nav-tabs-custom">
        <?php $this->beginBlock('About'); ?>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'username',
                    'name',
                    'email',
                ],
            ]); ?>
          
            <?php $this->endBlock(); ?>

            <?= Tabs::widget(
                [
                    'id' => 'relation-tabs',
                    'encodeLabels' => false,
                    'items' => [[
                        'label' => '<b>About</b>',
                        'content' => $this->blocks['About'],
                        'active' => true,
                    ],]
                ]
            );
            ?>
      </div>
            
      <!-- /.nav-tabs-custom -->
    </div>
    <!-- /.col -->
</div>
