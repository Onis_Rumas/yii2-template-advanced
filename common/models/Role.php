<?php

namespace common\models;

use common\models\base\Role as BaseRole;

/**
 * This is the model class for table "role".
 */
class Role extends BaseRole
{
    const FREE = 1;
    const PERSONAL = 2;
    const PROFESSIONAL = 3;
}
