<?php
/**
 * Created by PhpStorm.
 * User: feb
 * Date: 1/2/19
 * Time: 3:52 PM
 */

namespace common\components;


use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberUtil;

class PhoneNumberHelper
{
    public static function convertNumber($number)
    {
        $phoneUtil = PhoneNumberUtil::getInstance();
        try {
            $swissNumberProto = $phoneUtil->parse($number, "ID");
            return $swissNumberProto->getCountryCode() . $swissNumberProto->getNationalNumber();
        } catch (NumberParseException $e) {
            return null;
        }
    }
}