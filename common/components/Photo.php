<?php

namespace common\components;

use Yii;
use yii\helpers\Url;

class Photo
{    
    public static function get($file)
    {
        $file = trim($file);
        if (substr($file, 0, 4) == "http") {
            return $file;
        }
        if (file_exists(\Yii::getAlias("@frontend/web/uploads/" . $file)) && is_file(\Yii::getAlias("@frontend/web/uploads/" . $file))) {
            return Url::to(["/uploads/" . $file], true);
        } else {
            return Url::to(["/images/logo_square.png"], true);
        }
    }
}