<?php

namespace common\components;

use common\models\Menu;
use common\models\RoleMenu;
use Yii;
use yii\bootstrap\Widget;

class SidebarMenu extends Widget
{
    public static function getMenu($roleId, $parentId = NULL)
    {
        $hasActive = false;
        $output = [];
        /** @var Menu $menu */
        foreach (Menu::find()->where(["parent_id" => $parentId])->orderBy("`order` ASC")->all() as $menu) {
            //$icon = substr($menu->icon, 6);

            $status = Yii::$app->controller->id == $menu->controller;

            $obj = [
                "label" => $menu->name,
                "icon" => $menu->icon,
                "url" => SidebarMenu::getUrl($menu),
                "visible" => SidebarMenu::roleHasAccess($roleId, $menu->id),
                "active" => $status
            ];

            if($status){
                $hasActive = true;
            }

            if (count($menu->menus) != 0) {
                $children = SidebarMenu::getMenu($roleId, $menu->id);
                $obj["items"] = $children["menus"];
                if($children["active"]){
                    $obj["active"] = true;
                }
            }

            $output[] = $obj;
        }
        return ["menus" => $output, "active" => $hasActive];
    }

    private static function roleHasAccess($roleId, $menuId)
    {
        $roleMenu = RoleMenu::find()->where(["menu_id" => $menuId, "role_id" => $roleId])->one();
        if ($roleMenu) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    private static function getUrl($menu)
    {
        if ($menu->controller == NULL) {
            return "#";
        } else {
            return [$menu->controller . "/" . $menu->action];
        }
    }
}